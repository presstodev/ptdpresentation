Quick README how to run PTD Presentation Draft project:

1. `git clone https://gitlab.com/presstodev/ptdpresentation.git`
2. `cd ./ptdpresentation/docker`
3. `docker-compose up` (-d if in background)
4. `docker exec -it docker_ptd_draft_1 bash`
5. composer create-project -n (-n switch is needed because we are using private 3rd party bundles accessible via ssh key) (5.1) if at this step some access issue will occure, try to run `eval "$(ssh-agent -s)"; ssh-add /root/.ssh/id_rsa` inside docker container
6. after automatically executed `php bin/console c:cl` we need to make sure that our `.env` file is correct: 
        a) (usually not commiting .env.dist with proper credentials obviously) basically check if .env's content is same as .env.dist
        b) run `php bin/console ptd:core:install` - while executing we will be asked to provide password, unluckily twice, if we count user creation (working on it),
           and after that above-mentioned in user creation sub-command we will be asked to provide email, username and password (at least one big letter, one small, one number and one special sign)
           
           
 And this should be all. By Default admin panel should be accessible at URI: http://localhost:83/admin/dashboard (credentials same as given in `ptd:core:install`).